import React, { useContext } from 'react'
import './Searcher.css'

import { SearcherContext } from '../context/SearcherContext'

export default function Searcher() {

  const [searchValue, setSearchValue] = useContext(SearcherContext)
  
  return (
    <div className="search-input-content">
      <input value={searchValue} placeholder="Type to search.." onChange={ e => setSearchValue(e.target.value)}/>
    </div>
  )
}
