import { useState, useEffect } from 'react'

export const useDebounce = <T>(value: T, delay?: number) => {
  const [debounceValue, setDebounceValue] = useState(value);

  useEffect(() => {
    const timer = setTimeout(() => setDebounceValue(value), delay);

    return () => clearTimeout(timer);
  }, [value, delay]);

  return debounceValue;
}

export function titleCase(str: string) {
  return str.slice(0,1).toUpperCase() + str.slice(1).toLowerCase(); 
}