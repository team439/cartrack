import React, { useState, createContext}  from 'react'

const initalValue = ""
export const SearcherContextProvider = (props: { children: React.ReactChild | React.ReactFragment }) => {
  const [ searchValue, setSearchValue ] = useState<string>(initalValue)
  return (
    <SearcherContext.Provider value={[searchValue, setSearchValue]}>
      { props.children }
    </SearcherContext.Provider>
  )
}

export const SearcherContext = createContext(["", (searchValue: string) => {}])


