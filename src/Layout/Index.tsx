import React from 'react'
import Header from 'Layout/Header'
import List from 'Layout/List'
import { SearcherContextProvider } from 'context/SearcherContext'

export default function Index() {
  return (
    <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
      <SearcherContextProvider>
        <Header />
        <List />
      </SearcherContextProvider>
    </div>
  )
}
