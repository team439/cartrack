import React, { useState, useEffect, useContext } from 'react'
import UserType from 'types/UserType'
import { SearcherContext } from 'context/SearcherContext'
import { useDebounce, titleCase } from 'utils'

const FetchUrl = `${process.env.REACT_APP_BASE_URL}/users`

export default function List() {

  const [searchValue] = useContext(SearcherContext)

  const [List, setList] = useState<UserType[]>([])
  const [displayList, setDisplayList] = useState<UserType[]>([])

  const [isNodata, setIsNodata] = useState(false);

  const debounseValue = useDebounce(searchValue, 1000)

  useEffect(() => {
    if(!debounseValue) return

    const res = List.filter((item)=> {
      // @ts-ignore
      const titleValue = titleCase(debounseValue)
      return item.name.includes(titleValue) || item.email.includes(titleValue)
    })
    if(res.length) {
      setIsNodata(false)
      setDisplayList(res)
    }else {
      setIsNodata(true)
    }

  }, [debounseValue])

  useEffect(() => {
    fetch(FetchUrl).then(
      async (response) => {
        if (response.ok) {
          setList(await response.json());
        }
      }
    );  
  }, [])
   
  return (
    <div className="list-content">
      {
        isNodata ? <h3>No results found</h3> : displayList.length ? (
          <table 
            cellSpacing="0"
            cellPadding="0"
          >
            <thead >
              <tr>
                {
                  Object.keys(List[0]).map(item => {
                    return (
                      <th key={item}>{item}</th>
                    )
                  })
                }
              </tr>
            </thead>
            <tbody>
              { 
                displayList.map(item => {
                  return (
                    <tr key={item.id}>
                      <td>{item.id}</td>
                      <td>{item.name}</td>
                      <td>{item.username}</td>
                      <td><a href={`mailto:${item.email}`}>{item.email}</a></td>
                      <td>{item.address.street}</td>
                      <td><a href={`tel:${item.email}`}>{item.phone}</a></td>
                      <td>{item.website}</td>
                      <td>{item.company.name}</td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
        ) : null
      }
     </div>
  )
}
