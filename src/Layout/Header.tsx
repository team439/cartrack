import React from 'react'
import Searcher from 'component/Searcher'

export default function Header() {
  return (
    <>
      <h1 style={{marginTop: '20px'}}>What do you want?</h1>
      <Searcher/>
    </>
  )
}
