import { renderHook, act } from '@testing-library/react-hooks'

import { useDebounce, titleCase } from 'utils'

describe('Test useDebounce', () => {
  it('test hook', async () => {
    jest.useFakeTimers();

    const delay = 1300
    let { result, rerender } = renderHook(() => useDebounce("cc", delay))

    expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), delay);

    rerender("sd")

    expect(setTimeout).toHaveBeenCalledTimes(2);

    expect(result.current).toEqual("cc")
  })
})

describe('Test titleCase', () => {

  it('test if all lowCase', () => {

    const result = titleCase("acb")
    expect(result).toEqual("Acb")
  })
  
  it('test if all upperCase', () => {

    const result = titleCase("CCB")
    expect(result).toEqual("Ccb")
  })
})
